package com.userResgistration;

import java.sql.*;

import javax.persistence.*;



@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true, length = 45)
	private String email;
	
	@Column(nullable = false, length = 20)
	private String firstname;
	
	@Column(nullable = false, length = 20)
	private String lastname;
	
	@Column(nullable = false, length = 64)
	private String password;
	
	@Column(nullable = false, length = 20)
	private Integer contact;
	
	@Column(nullable = false)
	private String address;
	
//	@Column(nullable = false)
//	private Date DOB;
//
//	@Column(nullable = false)
//	private Date DOJ;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public Integer getContact() {
		return contact;
	}
	public void setContact(Integer contact) {
		this.contact = contact;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
//	public Date getDOB() {
//		return DOB;
//	}
//	public void setDOB(Date dOB) {
//		DOB = dOB;
//	}
//	public Date getDOJ() {
//		return DOJ;
//	}
//	public void setDOJ(Date dOJ) {
//		DOJ = dOJ;
//	}
	
	
}
